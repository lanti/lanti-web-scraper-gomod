.POSIX:

# https://gist.github.com/turtlemonvh/38bd3d73e61769767c35931d8c70ccb4
# https://gist.github.com/sohlich/8432e7c1bd56bc395b101d1ba444e982
# https://www.gnu.org/software/make/manual/html_node/Flavors.html#Flavors
# https://www.gnu.org/software/make/manual/html_node/Setting.html#Setting

# Go parameters
CC ::= go
# BUILD ::= $(CC) build -v -x
BUILD ::= $(CC) build -v
CLEAN ::= $(CC) clean
TEST ::= $(CC) test
GET ::= $(CC) get -u -v

VERSION ::= 1.0.0
COMMIT ::= $(shell git rev-parse HEAD)
BRANCH ::= $(shell git rev-parse --abbrev-ref HEAD)
CREATED_AT ::= $(shell date +%FT%T%z)

# Directories
CURRENT_DIR ::= $(shell pwd)
BUILD_DIR ::= $(CURRENT_DIR)/build

# Setup the -ldflags option for go build here, interpolate the variable values
LDFLAGS ::= -ldflags "-X main.version=$(VERSION) -X main.commit=$(COMMIT) -X main.branch=$(BRANCH) -X main.createdAt=$(CREATED_AT)"

all: clean build

################################################################################

build:
	cd $(CURRENT_DIR); \
	$(BUILD) $(LDFLAGS) ./... ; \
	$(BUILD) -o $(BUILD_DIR)/main $(LDFLAGS) $(CURRENT_DIR)/main.go

run:
	cd $(BUILD_DIR); \
	./main

################################################################################

clean:
	$(shell rm -f $(BUILD_DIR)/main)

.PHONY: clean build
