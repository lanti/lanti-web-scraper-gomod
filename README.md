# lanti-web-scraper-gomod

# This project created with the following commands

```sh
# Init project
$ go mod init gitlab.com/lanti/lanti-web-scraper-gomod
# See what extra dependencies my project requires
$ go mod tidy
# See why a dependency is required
$ go mod why -m rsc.io/quote
# Add missing dependencies
$ go build ./...
# test the module (if there's any test)
$ go test ./...
```

## Build & Run

```sh
$ go build -o build/main
$ ./build/main
```

https://github.com/golang/go/wiki/Modules

https://tip.golang.org/cmd/go/#hdr-Modules__module_versions__and_more
