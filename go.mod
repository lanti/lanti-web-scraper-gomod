module gitlab.com/lanti/lanti-web-scraper-gomod

require (
	github.com/PuerkitoBio/goquery v1.4.1
	github.com/andybalholm/cascadia v1.0.0 // indirect
	golang.org/x/net v0.0.0-20181108082009-03003ca0c849
	rsc.io/quote v1.5.2
)
