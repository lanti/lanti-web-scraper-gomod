package main

import (
	"fmt"
	"log"

	"rsc.io/quote"

	src "gitlab.com/lanti/lanti-web-scraper-gomod/middlewares"
)

var (
	version   string
	commit    string
	branch    string
	createdAt string
)

func main() {
	// These ones setted in Makefile
	log.Printf("main.version=%s", version)
	log.Printf("main.commit=%s", commit)
	log.Printf("main.branch=%s", branch)
	log.Printf("main.createdAt=%s", createdAt)

	fmt.Println(quote.Hello())

	/*
		- If last number same to 9999999 => do nothing!
	*/

	// Read line, best solution: https://golang.org/pkg/bufio/#Scanner

	/*func printSlice(s []int) {
		fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
	}*/
	fmt.Println("Web scraping job started...")
	src.Looper(1, 9999999)
}
