package middlewares

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

// scraper : returning content from html (with TOR proxy)
//func scraper(urlEnd string) (string, err error) {
func scraper(urlEnd string) []string { // TODO: Return []byte("string")
	slice := make([]string, 0)

	/*doc, err := goquery.NewDocument(URLStart + urlEnd)
	if err != nil {
		log.Fatal(err)
	}*/
	/*r := bytes.NewReader(TorProxy(URLStart + urlEnd))
	doc, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		log.Fatal(err)
	}*/

	var doc *goquery.Document
	if TOR == true {
		var err error
		r := bytes.NewReader(TorProxy(URLStart + urlEnd))
		doc, err = goquery.NewDocumentFromReader(r)
		Check(err)
	} else {
		var err error
		doc, err = goquery.NewDocument(URLStart + urlEnd)
		Check(err)
	}

	doc.Find(ElemParent).Each(func(i int, s *goquery.Selection) {
		data := s.Find(ElemChild).Text()
		slice = append(slice, data)
	})
	return slice
}

// IsExist : Checks if file exists
func IsExist(filename string) bool {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return false
	}

	file, err := os.Stat(filename)
	if err != nil {
		log.Fatal(err)
	}
	size := file.Size()
	fmt.Printf("The file is %d bytes long\n", size)
	if size == 0 {
		return false
	}

	return true
}

// Looper : Loops through numbers within range
func Looper(start int, end int) (string, err error) {
	if IsExist("result.csv") == true {
		fmt.Println("TRUE: file does exist")
		tail := Tail("result.csv")
		fmt.Printf("The last line was: %d\n", tail)
		start = tail + 1
	} else {
		fmt.Println("FALSE: file does not exist")
	}

	for i := start; i <= end; i++ {
		// If the file doesn't exist, create it, or append to the file
		// https://golang.org/pkg/os/#pkg-constants
		f, err := os.OpenFile("result.csv", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		Check(err)

		num := fmt.Sprintf("%07d", i)
		a := strings.Join(scraper(num), "\t") // Convert string slice to string
		b := fmt.Sprintf("%s\n", a)

		// Printing the current time to the console
		t := time.Now()
		fmt.Printf("[%s]\n", t.Format("2006-01-02 15:04:05"))

		NotACompany := "%TAXNR%\t%REGISTRATIONNR%\t%CREFOID%\t%NAME%\t%SHORTNAME%\t%LEGALFORM%\t%CITYLINK%\t%ADDRESS%\t%WEB%\t%MAINACTIVITY%\t%FOUNDEDDATE%\t%CAPITAL%\t%LASTACCOUNTDATE%\t%LASTTURNOVER%\t%LASTTURNOVEREUR%\t%LASTEMPLOYEEDATE%\t%LASTEMPLOYEE%\n"
		NotACompany2 := "%TAXNR%\t%REGISTRATIONNR%\t%CREFOID%\t%LEGALFORM%\t%CITYLINK%\t%MAINACTIVITY%\t%FOUNDEDDATE%\t%LASTACCOUNTDATE%\t%LASTTURNOVER%\t%LASTTURNOVEREUR%\t%LASTEMPLOYEEDATE%\t%LASTEMPLOYEE%\n"

		if (b != NotACompany) && (b != NotACompany2) && (b != "\n") {
			w := bufio.NewWriter(f)
			n, err := w.WriteString(b)
			Check(err)

			// Printing the results to the console
			fmt.Printf("%s", b)
			fmt.Printf("[wrote %d bytes]\n", n)

			w.Flush()
			if err := f.Close(); err != nil {
				log.Fatal(err)
			}
		}

		time.Sleep(CrawlDelay * time.Second)
		fmt.Println()
	}
	return
}
